Instalation process

1. Create in you mysql database the "contacts" database {utf-8}.
2. Update symfony vendor using composer update(During the update fil the database access details)
3. Update the database schema with console command from project main directory "php app/console doctrine:schema:create"
4. Copy assets from src to web folder using console command from project main directory "php app/console assets:install"
5. Clear the cache of application using console command from project main directory "php app/console cache:clear --env=prod --no-warmup"
6. Create virtual host to the web folder for example in my xamp is (C:\xampp\htdocs\www\codeexample\web)
   with url contacts.loc(Its important that url would have only one part)
7. Application should be ready to use.