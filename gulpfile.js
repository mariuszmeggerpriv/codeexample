'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var uglify = require('gulp-uglify');
var pump = require('pump');

gulp.task('sass', function () {
  return gulp.src('src/ContactsBundle/Resources/public/css/sass/**/*.scss')
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(gulp.dest('src/ContactsBundle/Resources/public/css/'));
});

gulp.task('minify-css', function() {
    return gulp.src('src/ContactsBundle/Resources/public/css/*.css')
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('src/ContactsBundle/Resources/public/css/css-compiled/'));
});

gulp.task('compress', function (cb) {
    pump([
        gulp.src('src/ContactsBundle/Resources/public/js/*.js'),
        uglify(),
        gulp.dest('src/ContactsBundle/Resources/public/js/js-compiled')
    ],
        cb
    );
});

gulp.task('sass:watch', function () {
  gulp.watch('src/ContactsBundle/Resources/public/css/sass/**/*.scss', ['sass', 'minify-css']);
});
