<?php

namespace ContactsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use ContactsBundle\Entity\Contact;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Component\HttpFoundation\Response;

class ContactsController extends Controller
{
    /**
     * @Route("/{group}",
     * defaults={"group": ""},
     * )
     *
     * Main controler with group filter parameter
     */
    public function indexAction($group)
    {
        $em = $this->getDoctrine()->getManager();
        if($group){
            $sql = "SELECT * FROM contact WHERE groups LIKE '%".$group."%'";
        } else{
            $sql = "SELECT * FROM contact ";
        }
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();
        $returnData = array();
        foreach($data as $contact){
            if($contact['phone']){
                $contact['phone'] = substr($contact['phone'], 0, -9). ' '.
                    substr($contact['phone'], 2, 3). '-'.
                    substr($contact['phone'], 5, 3). '-'.
                    substr($contact['phone'], 8, 3) ;
            }
            $returnData[] =  $contact;
        }

        $contacts = array('contacts'=>$returnData);
        return $this->render('ContactsBundle:Contacts:index.html.twig', $contacts);
    }

    /**
     * @Route("contacts/save")
     *
     * Ajax saving contact controller
     */
    public function contactSaveAction(Request $request)
    {
      if($request->isXmlHttpRequest()) {
          $data = $request->request->all();
          $contact = new Contact();
          $contact->setName(trim($data['name']));
          $contact->setSurname(trim($data['surname']));
          $contact->setEmail(trim($data['email']));
          $contact->setPhone(trim($data['phone']));
          if(isset($data['groups'])){
              $data['groups'] = $data['groups'];
          }else{
              $data['groups'] = array('Default');
          }
          $contact->setGroups($data['groups']);
          $em = $this->getDoctrine()->getManager();
          $em->persist($contact);
          $em->flush();
          die;
      }
    }

    /**
     * @Route("contacts/edit/get/{id}")
     *
     * Ajax getting single contact controller
     */
    public function getContactForEditAction($id, Request $request)
    {
        // $contact = $this->getDoctrine()->getRepository('ContactsBundle:Contact')->findOneById($id);
        if($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();
            $sql = "SELECT * FROM contact WHERE id = '".$id."'";
            $stmt = $em->getConnection()->prepare($sql);
            $stmt->execute();
            $data = $stmt->fetchAll();
            if($data){
              $response = new Response();
              $response->setContent(json_encode(array(
                  'data' => $data,
              )));
              $response->headers->set('Content-Type', 'application/json');
            }
            return $response;
        }
    }

    /**
     * @Route("contacts/edit")
     *
     * Ajax editing single contact controller
     */
    public function contactEditAction(Request $request)
    {
      if($request->isXmlHttpRequest()) {
        $data = $request->request->all();
        $contact = $this->getDoctrine()->getRepository('ContactsBundle:Contact')->findOneById($data['id']);
        $contact->setName(trim($data['name']));
        $contact->setSurname(trim($data['surname']));
        $contact->setEmail(trim($data['email']));
        $contact->setPhone(trim($data['phone']));
          if(isset($data['groups'])){
              $data['groups'] = $data['groups'];
          }else{
              $data['groups'] = array('Default');
          }
        $contact->setGroups($data['groups']);
        $em = $this->getDoctrine()->getManager();
        $em->flush();
        die;
      }
    }
    /**
     * @Route("contacts/delete")
     *
     * Ajax deleting single contact controller
     */
    public function contactDeleteAction(Request $request)
    {
      if($request->isXmlHttpRequest()) {
        $data = $request->request->all();
        $contact = $this->getDoctrine()->getRepository('ContactsBundle:Contact')->findOneById($data['id']);
        if($contact){
            $em = $this->getDoctrine()->getManager();
            $em->remove($contact);
            $em->flush();
            echo 'Contact Removed';
        }
        die;
    }
  }
}
