$('document').ready(function(){

    var app = {

        // Data container

        data : {
            groupsForInsert : [],
            groupsForEdit : [],
            editId : 0,
            errorMessages : []
        },

        // data table init and setting selected in groups filter

        defaultLoad : function(){
            $('.contactsTable').DataTable();
            var paramaters = window.location.pathname.split( '/' )
            $('#contactGroupFilter').find('option').each(function(){
                if($(this).val() == paramaters[1]){
                    $(this).attr('selected', 'selected');
                }
            })
        },

        // group mechanism in edit modal

        modalEditGroupMachine : function(){
            var groups = app.data.groupsForEdit
            var select = $('.editContactGroup');
            var addButton = $('.addEditGroup');

            select.change(function(){
                var groupName = select.val();

                if(groupName == 0){
                    $(this).next().find('i').removeClass('fa-plus').addClass('fa-ban').attr('title', 'Remove from all groups');
                }else{
                    $(this).next().find('i').removeClass('fa-ban').addClass('fa-plus').attr('title', 'Add to Group (max 3)');;
                }
                addButton.show();
            })
            addButton.click(function(){
                var groupName = select.val();
                if (groupName == 0){
                    $(this).parent().siblings().find('.groupAlert').remove();
                    groups = [];
                    app.data.groupsForEdit = [];
                    select.find('option').each(function(){
                        $(this).removeAttr('disabled');
                    })
                }else{
                    if(groupName != null && groups.length < 3){
                        if($.inArray(groupName,groups) == -1){
                            app.data.groupsForEdit.push(groupName);
                            select.find('option').each(function(){
                                if($(this).val() == groupName){
                                    $(this).attr('disabled','disabled');
                                }
                            })
                            $('.groupEditAlertWrapper').append('<div class="alert alert-info alert-dismissible groupAlert" role="alert"><button type="button" class="close removeEditGroup" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+groupName+'</div>')
                        }
                    }
                }

            })
            $('.modal').on('click','.removeEditGroup',function(){
                var removedGroup = $(this).parent().not('span').text().substr(1);
                var index = groups.indexOf(removedGroup);
                if (index > -1) {
                    select.find('option').each(function(){
                        if($(this).val() == removedGroup){
                            $(this).removeAttr('disabled');
                        }
                    })
                    groups.splice(index, 1);
                }

            })
        },

        // group mechanism in add modal

        modalAddGroupMachine : function(){
            var groups = app.data.groupsForInsert
            var select = $('.contactGroup');
            var addButton = $('.addGroup');
            if(select.val() == 0){
                addButton.hide();
            }

            select.change(function(){
                var groupName = select.val();

                if(groupName == 0){
                    $(this).next().find('i').removeClass('fa-plus').addClass('fa-ban').attr('title', 'Remove from all groups');
                }else{
                    $(this).next().find('i').removeClass('fa-ban').addClass('fa-plus').attr('title', 'Add to Group (max 3)');;
                }
                addButton.show();
            })
            addButton.click(function(){
                var groupName = select.val();
                if (groupName == 0){
                    $(this).parent().siblings().find('.groupAlert').remove();
                    groups = [];
                    app.data.groupsForInsert = [];
                    select.find('option').each(function(){
                        $(this).removeAttr('disabled');
                    })
                }else{
                    if(groupName != null && groups.length < 3){
                        if($.inArray(groupName,groups) == -1){
                            app.data.groupsForInsert.push(groupName);
                            select.find('option').each(function(){
                                if($(this).val() == groupName){
                                    $(this).attr('disabled','disabled');
                                }
                            })
                            $('.groupAlertWrapper').append('<div class="alert alert-info alert-dismissible groupAlert" role="alert"><button type="button" class="close removeGroup" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+groupName+'</div>')
                        }
                    }
                }

            })
            $('.modal').on('click','.removeGroup',function(){
                var removedGroup = $(this).parent().not('span').text().substr(1);
                var index = groups.indexOf(removedGroup);
                if (index > -1) {
                    select.find('option').each(function(){
                        if($(this).val() == removedGroup){
                            $(this).removeAttr('disabled');
                        }
                    })
                    groups.splice(index, 1);
                }
            })

        },

        // adding contact

        insertContact : function(){
            app.modalAddGroupMachine();
            $('.addCategorySubmit').click(function(){
                var form = $('.addCategoryModal form');
                var name = $('.addCategoryModal .contactName').val();
                var surname = $('.addCategoryModal .contactSurName').val();
                var email = $('.addCategoryModal .contactEmail').val();
                var phone = $('.addCategoryModal .contactPhone').val();
                var contactGroups = app.data.groupsForInsert;
                var validate = app.validateForm(name, surname, email, phone, form);
                if(!validate){
                    return false;
                }

                $.ajax({
                    method: "POST",
                    url: "http://"+window.location.hostname+"/contacts/save",
                    data: {
                        name: name,
                        surname: surname,
                        email: email,
                        phone: phone,
                        groups: contactGroups
                    }
                })
                    .done(function( msg ) {
                        $('.addCategoryModal').modal('hide');
                        window.location = "http://"+window.location.hostname+"/";
                    }).fail(function(msg) {
                        alert( 'Category couldnt be saved try again:');
                    });

            })
        },

        // deleting contact

        deleteContact : function(){
            $('.deleteContact').click(function(){
                var that = $(this);
                var id = that.data('id');
                $('#confirm').modal({ backdrop: 'static', keyboard: false })
                    .one('click', '.deleteContactConfirm', function() {
                        $.ajax({
                            method: "POST",
                            url: "http://"+window.location.hostname+"/contacts/delete",
                            data: {
                                id: id
                            }
                        })
                            .done(function( msg ) {
                                that.parent().parent().parent().hide(400);
                                $('.addCategoryModal').modal('hide');
                            }).fail(function(msg) {
                                alert( 'Category couldnt be deleted try again: ');
                            });
                    })
            });
        },

        // editing contact

        editContact : function(){
            $('.editContact').click(function(){

                var select = $('.editContactGroup');
                select.find('option').each(function(){
                    $(this).removeAttr('disabled');
                })
                app.data.errorMessages = [];
                var form = $('.editCategoryModal form');
                form.find('.errorMsgContainer').remove();
                app.data.groupsForEdit = [];
                var that = $(this);
                var id = that.data('id');
                app.data.editId = id;

                $.ajax({
                    method: "GET",
                    url: "http://"+window.location.hostname+"/contacts/edit/get/"+id,
                    data: {
                        id: id
                    }
                })
                    .done(function( data ) {
                        $('.editContactName').val(data.data[0].name);
                        $('.editContactSurName').val(data.data[0].surname);
                        $('.editContactEmail').val(data.data[0].email);
                        $('.editContactPhone').val(data.data[0].phone);
                        app.data.groupsForEdit = data.data[0].groups.split(',');
                        app.writeGroups();
                        $('.editCategoryModal').modal('show');
                    }).fail(function(msg) {
                        alert( 'Category couldnt be seleted from database try again');
                    });
            })
            $('.editCategorySubmit').click(function(){
                var form = $('.editCategoryModal form');
                var name = $('.editCategoryModal .editContactName').val();
                var surname = $('.editCategoryModal .editContactSurName').val();
                var email = $('.editCategoryModal .editContactEmail').val();
                var phone = $('.editCategoryModal .editContactPhone').val();
                var contactGroups = app.data.groupsForEdit;

                var validate = app.validateForm(name, surname, email, phone, form);
                if(!validate){
                    return false;
                }
                $.ajax({
                    method: "POST",
                    url: "http://"+window.location.hostname+"/contacts/edit",
                    data: {
                        id:app.data.editId,
                        name: name,
                        surname: surname,
                        email: email,
                        phone: phone,
                        groups: contactGroups
                    }
                })
                    .done(function( msg ) {
                        app.data.editId = 0;
                        $('.editCategoryModal').modal('hide');
                        window.location = "http://"+window.location.hostname+"/";

                    }).fail(function(msg) {
                        alert( 'Category couldnt be saved try again: ');
                    });
            })

        },

        //writing groups boxes in edit modal

        writeGroups : function(){
            var select = $('.editContactGroup');
            var alertWrapper = $('.groupEditAlertWrapper');
            alertWrapper.children().remove();
            for(var i = 0; i < app.data.groupsForEdit.length; i++){
                if(app.data.groupsForEdit[i] != 'Default'){
                    alertWrapper.append('<div class="alert alert-info alert-dismissible groupAlert" role="alert"><button type="button" class="close removeEditGroup" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+app.data.groupsForEdit[i]+'</div>')
                }else{
                    var index = app.data.groupsForEdit.indexOf('Default');
                    if (index > -1) {
                        app.data.groupsForEdit.splice(index, 1);
                    }
                }
                select.find('option').each(function(){
                    if($(this).val() == app.data.groupsForEdit[i]){
                        $(this).attr('disabled','disabled');
                    }
                })
                app.modalEditGroupMachine();
            }
        },

        // redirecting to url with group form group filter

        filterByGroup : function(){
            $('#contactGroupFilter').change(function(){
                var filterGroup = $(this).val();
                window.location = "http://"+window.location.hostname+"/" + filterGroup;
            })
        },

        // validating modal forms

        validateForm : function(name, surname, email, phone, form){
            app.data.errorMessages = [];
            form.find('.errorMsgContainer').remove();
            if(name.trim() == ''){
                errorMsg = 'Name is required';
                app.data.errorMessages.push(errorMsg)
                form.find('.name').after('<span class="text-danger errorMsgContainer">'+errorMsg+'</span>')
            }
            if(surname.trim() == ''){
                errorMsg = 'Surname is required';
                app.data.errorMessages.push(errorMsg)
                form.find('.surname').after('<span class="text-danger errorMsgContainer">'+errorMsg+'</span>')
            }
            if(email.trim() == ''){
                errorMsg = 'Email is required';
                app.data.errorMessages.push(errorMsg)
                form.find('.email').after('<span class="text-danger errorMsgContainer">'+errorMsg+'</span>')
            }
            else{
                var pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/
                if(!email.match(pattern)){
                    errorMsg = 'Wrong email patern';
                    app.data.errorMessages.push(errorMsg)
                    form.find('.email').after('<span class="text-danger errorMsgContainer">'+errorMsg+'</span>')
                }
            }
            if(phone.trim().length > 0){
                if(phone.trim().length != 11){
                    errorMsg = 'Wrong phone number patern. Exactly 11 numbers required';
                    app.data.errorMessages.push(errorMsg)
                    form.find('.phone').after('<span class="text-danger errorMsgContainer">'+errorMsg+'</span>')
                }
                else if(!$.isNumeric(phone.trim())){
                    errorMsg = 'Wrong phone patern only numbers are accepted';
                    app.data.errorMessages.push(errorMsg)
                    form.find('.phone').after('<span class="text-danger errorMsgContainer">'+errorMsg+'</span>')
                }
            }
            if(app.data.errorMessages.length > 0){
                return false;
            }else{
                return true;
            }

        }

    }

    app.defaultLoad();
    app.insertContact();
    app.deleteContact();
    app.filterByGroup();
    app.editContact();
})
